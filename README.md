Chess Playground
========

## Summary

Write your own Chess AI evaluation function! This is a far simpler project than full blown chess AI. Anybody can do it!

## Description

chess-playground is chess AI scaffolding that aims to be as simple as possible. This project is designed for AI instruction for people with little programming experience. Unlike other Chess AI projects I found, the top priorities of this project are to:

* Installation is as simple as possible.
* Running the project is as simple as possible.
* Minimize the number of files and folders.
* Minimize dependencies and features.
* Minimize the programming language specific knowledge required to write chess AI code.

This project is not designed to be used as a lesson on Java or installing dependencies. It's meant to be the simplest hands-on chess AI coding tournament possible.

## Setup

Setup may be easiest from Linux. You will need to git clone this project from a git command line:

	git clone https://github.com/Zulban/chess-playground

### Setup - Linux

After downloading the project with git clone, open the "chess-playground" folder, then open the "src" folder. Right click, and click 'open a terminal here'. Then type this command and press enter:

	bash setup-linux.sh

You may also be asked for a password. If so, type it in and press enter.

### Setup - Windows

Currently, setup on Linux is best supported. But to get this working on Windows, you will need to:

* Install 'Java JDK'
* Add Java to your classpath, [see here](https://www.google.ca/search?q=windows+javac+is+not+recognized+as+an+internal+or+external+command) for more information.
* Open up the Windows command prompt. Navigate to the "chess-playground" folder.

Type these commands into the command prompt:

	javac -sourcepath src -d build/classes src/console/ChessPlayground.java
	
	java -cp build\classes console.ChessPlayground score bots/TemplateBot.java data/demo-fens.txt

If you get these commands to work, then typing this in Windows:

    javac -sourcepath src -d build/classes src/console/ChessPlayground.java
    java -cp build\classes console.ChessPlayground
    
is equivalent to typing this in Linux:

    bash chess-playground.sh

You can then follow the instructions in the Linux section. Just view the contents of the ".sh" files.

## Usage (Linux)

To see a simple demo, type this command and press enter:

	bash demo.sh

Here's a more advanced example:

	bash chess-playground.sh score bots/TemplateBot.java ../data/demo-fens.txt

This command will score chess boards. It uses the bot found in "default_bots/TemplateBot.java" to examine all the chess games found in the folder "../data/demo-fens.txt".

Another example:

    bash chess-playground.sh play bots/TemplateBot.java random --compute-time=10 --log
    
This command plays one chess game - TemplateBot versus the random bot. It gives TemplateBot.java ten seconds (instead of the default five) to think about its turn. Finally, "--log" means it will write a file into the "logs" folder with the history of the game.

To read about all options and commands, run this:

	bash chess-playground.sh -h

## Writing your own bot

You will want to copy the "TemplateBot.java" file into the "bots" folder. Then rename it to your name like "StuartSpenceBot.java" and inside the file, replace "class TemplateBot" with "class StuartSpenceBot". Now if you want to use this new bot instead to play games, you can type this command:

    bash chess-playground.sh play bots/StuartSpenceBot.java random

### 1) Change the name

Name your bot! Change the "botName" and "studentName" values. "botName" can be anything you want, but "studentName" should be your real, full name.

### 2) Write the evaluation function

Change the contents of the function labeled "public float evaluate". Basically, "evaluate" is a function. When the ChessPlayground code gives your evaluate function a chess board, your evaluate function needs to give back a number. To give back a number is called returning a number. The number that your evaluate function needs to "return" must be high if white is doing well, and low if black is doing well.