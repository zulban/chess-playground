package console;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import ai.Features;
import chess.Board;
import chess.FEN;
import chess.Match;
import default_bots.BotLoader;
import default_bots.EvaluationBot;
import default_bots.HumanBot;
import default_bots.RandomBot;
import default_bots.TemplateBot;
import generic.Logging;
import generic.Utilities;

public class ChessPlayground extends Docopt {

	public static void printStars() {
		System.out.println("**********************************************************************");
	}

	private static void showBoardAndScoreFromFen(String fen, EvaluationBot bot) {
		printStars();
		Board board = FEN.getBoardWithFen(fen);
		System.out.println(board);
		float score = bot.evaluate(new Features(board));
		System.out.println("\n          score = " + score);
	}

	public static boolean runScoreFenTextFile(EvaluationBot bot, String fenPath) {
		File file = new File(fenPath);

		ArrayList<String> lines = new ArrayList<String>();
		try (BufferedReader br = new BufferedReader(new FileReader(file))) {
			String line;
			while ((line = br.readLine()) != null) {
				if (!line.startsWith("#") && line.trim().length() > 0) {
					lines.add(line);
				}
			}
		} catch (IOException e) {
			System.out.println("Score FEN text file failed. Not a file: '" + fenPath + "'");
			return false;
		}

		for (int i = 0; i < lines.size(); i++)
			showBoardAndScoreFromFen(lines.get(i).trim(), bot);
		return true;
	}

	private static EvaluationBot getBotFromLabel(String label) {
		if (label.equals("random"))
			return new RandomBot();
		else if (label.equals("human"))
			return new HumanBot();
		else {
			return BotLoader.getBotFromPath(label);
		}
	}

	public static void main(String[] args) {
		ChessPlayground.HELP = "Chess Playground!"
				+ "\n\nscore - use MyBot.java to evaluate the boards found in the FEN text file."
				+ "\nplay - play one match. The bot arguments can be 'human' where you choose"
				+ "\n       the moves, 'random', or a path to a bot java file like \"bots/SimpleBot.java\"."
				+ "\n       For example, you could type \"play random bots/MyBot.java\" which plays your"
				+ "\n       MyBot.java against a bot that plays random moves." + "\n\n    Options:"
				+ "\n--compute-time=<seconds>    Bots will have this many seconds to compute their turn. [default: 5]"
				+ "\n--verbose" + "\n--log          Write game logs to the 'logs' folder." + "\n\n    Usage:"
				+ "\njava ChessPlayground score <bot-java-file> <path-to-FEN-text-file> [options]"
				+ "\njava ChessPlayground play <bot1-java-file> <bot2-java-file> [options]"
				+ "\njava ChessPlayground robin <bots-folder> [options]";
		ChessPlayground.args = args;

		if (hasOption("-h") || hasOption("--help")) {
			showHelp();
			return;
		}

		if (hasUnknownOptions()) {
			System.out.println("Unknown command line argument: '" + getUnknownOptions() + "'");
			showHelp();
			return;
		}

		Logging.setOutputs(hasOption("--verbose"), hasOption("--log"));

		int computeTime = getIntArgument("--compute-time");

		if (hasCommand("score") && args.length >= 2) {
			EvaluationBot bot = getBotFromLabel(args[1]);
			if (bot == null)
				return;
			if (!runScoreFenTextFile(bot, args[2]))
				showHelp();
		} else if (hasCommand("play") && args.length >= 3) {
			EvaluationBot bot1 = getBotFromLabel(args[1]);
			EvaluationBot bot2 = getBotFromLabel(args[2]);
			if (bot1 == null || bot2 == null)
				return;

			Match match = new Match(bot1, bot2, computeTime, true, true);
			match.resolve();
			if (hasOption("--log"))
				match.save();

		} else if (hasCommand("robin") && args.length >= 2) {
			runRoundRobin(args[1], computeTime, hasOption("--log"), hasOption("--verbose"));
		} else {
			showHelp();
		}
	}

	private static void showRobinResults(ArrayList<EvaluationBot> bots, int[] wins, int[] losses, int[] draws,
			int[] points) {
		int gameCount = wins[0] + losses[0] + draws[0];

		ConsoleColor.printBlue("Ran " + gameCount + " matches.");
		int[] sortedPoints = Arrays.copyOf(points, points.length);
		Arrays.sort(sortedPoints);
		int rank = 0;
		int lastPoints = -1;
		for (int i = 0; i < bots.size(); i++) {
			int m = sortedPoints[bots.size() - i - 1];
			if (m == lastPoints)
				continue;
			lastPoints = m;

			for (int j = 0; j < bots.size(); j++) {
				if (m == points[j]) {
					rank++;
					String rankString;
					if (rank == 1)
						rankString = "FIRST";
					else if (rank == 2)
						rankString = "SECOND";
					else if (rank == 3)
						rankString = "THIRD";
					else
						rankString = Integer.toString(rank);
					EvaluationBot bot = bots.get(j);
					ConsoleColor
							.printGreen(rankString + ": '" + bot.getBotName() + "' by '" + bot.getAuthor() + "' with "
									+ wins[j] + "W " + losses[j] + "L " + draws[j] + "D and " + points[j] + " points.");
				}
			}
		}
	}

	private static void runRoundRobin(String botsFolder, int computeTime, boolean saveHistory, boolean verbose) {
		ArrayList<EvaluationBot> bots = BotLoader.getBotsFromFolder(botsFolder);
		ConsoleColor.printBlue("Starting a round robin tournament between " + bots.size() + " chess bots.");

		int[] wins = new int[bots.size()];
		int[] losses = new int[bots.size()];
		int[] draws = new int[bots.size()];
		int[] points = new int[bots.size()];

		int count = 0;
		int gameTotal = bots.size() * (bots.size() - 1);
		long startTime = System.currentTimeMillis();

		for (int i = 0; i < bots.size(); i++) {
			for (int j = 0; j < bots.size(); j++) {
				if (i == j)
					continue;
				count++;
				EvaluationBot whiteBot = bots.get(i);
				EvaluationBot blackBot = bots.get(j);

				ConsoleColor.printYellow("Starting game " + count + " of " + gameTotal);
				if (count > 1) {
					long timePassed = System.currentTimeMillis() - startTime;
					long averageDuration = timePassed / count;
					int estimatedSecondsLeft = (int) ((gameTotal - count) * averageDuration / 1000);
					ConsoleColor.printOrange("     Estimated seconds left: " + estimatedSecondsLeft);
				}
				
				Match match = new Match(whiteBot, blackBot, computeTime, verbose, false);
				match.resolve();
				if (saveHistory) {
					String whiteName = Utilities.getTruncatedBotNameForFilename(whiteBot.getAuthor());
					String blackName = Utilities.getTruncatedBotNameForFilename(blackBot.getAuthor());
					match.save(whiteName + "-" + blackName);
				}

				if (match.isDraw()) {
					draws[i]++;
					draws[j]++;
					points[i]++;
					points[j]++;
				} else {
					int winnerIndex, loserIndex;
					if (match.didThisBotWin(whiteBot)) {
						winnerIndex = i;
						loserIndex = j;
					} else {
						winnerIndex = j;
						loserIndex = i;
					}
					wins[winnerIndex]++;
					losses[loserIndex]++;
					points[winnerIndex] += 3;
				}
			}
		}

		showRobinResults(bots, wins, losses, draws, points);
	}

}
