package console;

public enum ConsoleColor {
	ALPHA(-1), RED(41), GREEN(42), ORANGE(43), BLUE(44), PURPLE(45), AQUA(46), SILVER(47), GREY(100), PINK(101), LIME(
			102), YELLOW(103), SKY(104), MAGENTA(105), TURQUOISE(106);

	private int value;

	private ConsoleColor(int value) {
		this.value = value;
	}

	public static void printRed(String text) {
		printColor(text, ConsoleColor.RED, true);
	}

	public static void printGreen(String text) {
		printColor(text, ConsoleColor.GREEN, true);
	}

	public static void printYellow(String text) {
		printColor(text, ConsoleColor.YELLOW, true);
	}

	public static void printOrange(String text) {
		printColor(text, ConsoleColor.ORANGE, true);
	}

	public static void printBlue(String text) {
		printColor(text, ConsoleColor.BLUE, true);
	}

	public static void printColor(String text, ConsoleColor color) {
		printColor(text, color, true);
	}

	public static void printColor(String text, ConsoleColor color, boolean newline) {
		if(newline)
			text=text.concat((newline ? "\n" : ""));
		if (canPrintBashColors()) {
			String toPrint = getEscapedColorText(text, color, newline);
			System.out.print(toPrint);
		} else {
			System.out.print(text);
		}
	}
	
	public static String getEscapedColorText(String text, ConsoleColor color, boolean newline) {
		return "\033[" + (color.getValue() - 10) + "m" + text + "\033[0m";
	}

	public static boolean canPrintBashColors() {
		// Returns true if the console is able to print with these bash colors.
		return OSValidator.isUnix();
	}

	public int getValue() {
		return value;
	}
}