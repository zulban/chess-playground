package console;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Docopt {
	protected static String HELP = "";
	protected static String[] args;

	public static void showHelp() {
		System.out.println(HELP);
	}

	protected static String getUnknownOptions() {
		// Returns a string with the first invalid/unknown command line
		// argument.
		// Returns an empty string if all appear valid.
		String[] lines = HELP.split("\n");
		for (int i = 0; i < args.length; i++) {
			String arg = args[i];
			if (!arg.startsWith("--"))
				continue;
			if (arg.contains("="))
				arg = arg.split("=")[0];
			boolean found = false;
			for (int j = 0; j < lines.length; j++) {
				String line = lines[j];
				if (line.startsWith(arg)) {
					found = true;
					break;
				}
			}
			if (!found) {
				return arg;
			}
		}
		return "";
	}

	protected static boolean hasUnknownOptions() {
		return getUnknownOptions().length() > 0;
	}

	protected static float getFloatDefault(String argument) {
		// given an argument like '--wait' returns the default value, or zero if
		// none is found.
		Pattern pattern = Pattern.compile(argument + ".*default:[ ]*([0-9]+(\\.[0-9]+)?)");
		Matcher matcher = pattern.matcher(HELP);
		if (matcher.find()) {
			return Float.parseFloat(matcher.group(1).toString());
		}
		return 0;
	}

	protected static int getIntArgument(String argument) {
		return (int) getFloatArgument(argument);
	}

	protected static float getFloatArgument(String argument) {
		for (int i = 0; i < args.length; i++) {
			String arg = args[i];
			if (arg.startsWith(argument) && arg.contains("=")) {
				String[] splits = arg.split("=");
				String suffix = splits[splits.length - 1];
				return Float.parseFloat(suffix);
			}
		}
		return getFloatDefault(argument);
	}

	protected static boolean hasCommand(String command) {
		// Returns true if the command line args contains this command, like
		// java Jarkanoid unittest
		for(int i=0;i<args.length;i++)
		    if(args[i].equals(command))
		        return true;
		return false;
	}

	protected static boolean hasOption(String flag) {
		// Returns true if the command line args contains this option, like
		// --verbose
		return Arrays.asList(args).contains(flag);
	}
}
