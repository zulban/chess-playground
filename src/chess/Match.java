package chess;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Scanner;

import ai.Features;
import ai.MinMaxResult;
import ai.TreeSearch;
import console.ConsoleColor;
import default_bots.EvaluationBot;
import default_bots.HumanBot;
import default_bots.RandomBot;

public class Match {
	private EvaluationBot whiteBot, blackBot;
	private ArrayList<String> fenHistory = new ArrayList<String>();
	private ArrayList<String> boardStringHistory = new ArrayList<String>();
	public Board board;

	private final int MAXIMUM_TURNS = 200;
	private int turnCount = 0;

	private Outcomes outcome = Outcomes.IN_PROGRESS;
	private boolean verbose;
	private int computeSeconds;
	private long expireTime;

	public Match(EvaluationBot bot1, EvaluationBot bot2, int computeSeconds, boolean verbose,
			boolean randomWhiteBlack) {
		if (Math.random() < 0.5 && randomWhiteBlack) {
			whiteBot = bot2;
			blackBot = bot1;
		} else {
			whiteBot = bot1;
			blackBot = bot2;
		}
		this.verbose = verbose;
		this.computeSeconds = computeSeconds;

		setBoard();
	}

	public String getBoardString() {
		return board.toString();
	}

	public boolean didThisBotWin(EvaluationBot queryBot) {
		if (outcome == Outcomes.IN_PROGRESS || isDraw())
			return false;

		if (queryBot == whiteBot)
			return outcome == Outcomes.WHITE_WIN || outcome == Outcomes.BLACK_RESIGN;
		return outcome == Outcomes.BLACK_WIN || outcome == Outcomes.WHITE_RESIGN;
	}

	public boolean isDraw() {
		return outcome == Outcomes.MAX_TURNS || outcome == Outcomes.STALEMATE || outcome == Outcomes.REPETITION;
	}

	public static void printStars() {
		System.out.println("**********************************************************************");
	}

	private void logBoard(Board toRecord) {
		fenHistory.add(FEN.getFEN(toRecord));
		boardStringHistory.add(toRecord.toString(false));

		if (verbose)
			System.out.println(toRecord);
	}

	private void logString(String toRecord) {
		boardStringHistory.add(toRecord);
		if (verbose)
			System.out.println(toRecord);
	}

	public void save() {
		save("");
	}

	public void save(String label) {
		// Java is a ridiculous language.
		String path = getNextLogPath(label);
		if (verbose)
			System.out.println("\n\nSaving match game log to: '" + path + "'");

		StringBuilder sb = new StringBuilder();

		for (int i = 0; i < boardStringHistory.size(); i++) {
			sb.append(boardStringHistory.get(i));
			sb.append("\n\n");
		}

		sb.append("\n\n");

		for (int i = 0; i < fenHistory.size(); i++) {
			sb.append(fenHistory.get(i));
			sb.append("\n");
		}

		try (PrintWriter out = new PrintWriter(path)) {
			out.println(sb.toString());
		} catch (FileNotFoundException e) {
			System.out.println("ERROR: Match.save failed.");
			e.printStackTrace();
		}
	}

	private String getNextLogPath() {
		return getNextLogPath("");
	}

	private String getNextLogPath(String label) {
		Date date = new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH);
		int day = cal.get(Calendar.DAY_OF_MONTH);
		String prefix = "../logs/" + year + "-" + month + "-" + day + "-game-";

		int index = 1;
		String path = "";
		while (true) {
			String labelSuffix = label.length() == 0 ? "" : "-" + label;
			path = prefix + String.format("%03d", index) + labelSuffix + ".log";
			File f = new File(path);
			if (!f.exists() && !f.isDirectory())
				break;
			index++;
		}
		return path;
	}

	public void playNextTurn(Move move) {
		board.move(move);
		updateMatchOutcome();
		logBoard(board);
	}

	public void playNextTurn() {
		turnCount++;

		if (verbose)
			printStars();
		EvaluationBot bot = board.getMyPlayer() == Player.WHITE ? whiteBot : blackBot;

		Move move;
		Board node = board.getCopyOfBoard();
		if (bot instanceof HumanBot) {
			move = getHumanMove(node);
		} else if (bot instanceof RandomBot) {
			move = getRandomMove(node);
		} else {
			move = getBestMove(node, bot);
		}

		logString(bot.getBotName() + " (" + board.getMyPlayer() + ") plays " + move);
		playNextTurn(move);
	}

	public void setBoard(String fen) {
		logString(whiteBot.getBotName() + " (white) by " + whiteBot.getAuthor() + " versus " + blackBot.getBotName()
				+ " (black) by " + blackBot.getAuthor());

		board = FEN.getBoardWithFen(fen);
		logBoard(board);
	}

	private void setBoard() {
		setBoard(Board.startFEN);
	}

	public boolean isRepetitionDraw() {
		// returns true if the exact current position has already occurred at
		// least two other times this game
		String currentFen = FEN.getFEN(board);
		int matchCount = 0;
		for (int i = 0; i < fenHistory.size(); i++) {
			if (fenHistory.get(i).equals(currentFen))
				matchCount++;
		}
		return matchCount >= 3;
	}

	public boolean updateMatchOutcome() {
		// Checks if a draw, win, or loss occurred, updates outcome.
		// returns true if the game is finished.

		if (board.isCheckmate() || board.getAllMoves().length == 0) {
			outcome = board.getMyPlayer() == Player.WHITE ? Outcomes.BLACK_WIN : Outcomes.WHITE_WIN;
		} else if (board.isStalemate()) {
			outcome = Outcomes.STALEMATE;
		} else if (isRepetitionDraw()) {
			outcome = Outcomes.REPETITION;
		}

		return isFinished();
	}

	public boolean isFinished() {
		return outcome != Outcomes.IN_PROGRESS;
	}

	public void resolve() {
		while (turnCount < MAXIMUM_TURNS) {
			playNextTurn();

			if (isFinished())
				break;
		}

		if (outcome == Outcomes.IN_PROGRESS)
			outcome = Outcomes.MAX_TURNS;

		logString(this.toString()+"\n");
	}

	private Move getRandomMove(Board node) {
		Move[] moves = node.getAllMoves();
		int index = (int) (Math.random() * moves.length);
		return moves[index];
	}

	private Move getHumanMove(Board node) {
		Scanner scanner = new Scanner(System.in);
		String moveString = "";
		Move move = new Move(0, 0, 0, 0);
		boolean isFirst = true;
		while (!Move.isValidMoveString(moveString) || !node.isValidMove(move)) {
			if (isFirst) {
				isFirst = false;
			} else {
				if (!Move.isValidMoveString(moveString))
					System.out.println(
							"Bad move! A move must be exactly four characters, representing two squares in a chess board.\nExample: \"e3e4\"");
				else if (!node.isValidMove(move))
					System.out.println(
							"Bad move! This piece cannot move there, or it doesn't belong to you, or there's no piece there.");
			}
			System.out.print("Enter your move: ");
			moveString = scanner.next();
			moveString = moveString.trim();
			move = Move.getMoveFromMoveString(moveString);
		}
		return move;
	}

	public String toString() {
		return "Match: '" + whiteBot.getBotName() + "' (white) versus '" + blackBot.getBotName()
				+ "' (black). Outcome: " + outcome;
	}

	private void print(String text) {
		System.out.println(text);
	}

	private Move getBestMove(Board board, EvaluationBot bot) {
		Move bestMove = null;

		// this is the Unix time when computations should stop, time is up.
		expireTime = System.currentTimeMillis() + computeSeconds * 1000;

		int depth = 0;
		boolean isMaxPlayer = board.getMyPlayer() == Player.WHITE;

		while (!TreeSearch.hasExpired(expireTime)) {
			depth++;
			MinMaxResult mmr = TreeSearch.minMax(board.getCopyOfBoard(), bot, depth, isMaxPlayer, expireTime);
			if (!TreeSearch.hasExpired(expireTime)) {
				if (verbose)
					System.out.println("Finished computing depth = " + depth);
				bestMove = mmr.bestMove;
			}
		}

		if (bestMove == null) {
			bestMove = board.getAllMoves()[0];
			ConsoleColor.printRed("Warning: getBestMove was only able to return the first move.");
		}
		return bestMove;
	}
}
