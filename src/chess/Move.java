package chess;

public class Move {
	int x1,y1,x2,y2;
	public Move(int x1,int y1,int x2, int y2){
		this.x1=x1;
		this.y1=y1;
		this.x2=x2;
		this.y2=y2;
	}
	
	public String toString(){
		return "Move "+FEN.getCharFromX(x1)+(y1+1)+" to "+FEN.getCharFromX(x2)+(y2+1);
	}
	
	public static boolean isValidMoveString(String moveString) {
		return getMoveFromMoveString(moveString) != null;
	}

	public static Move getMoveFromMoveString(String moveString) {
		if (moveString.length() != 4)
			return null;

		String a = moveString.substring(0, 2);
		String b = moveString.substring(2, 4);
		int x1, y1, x2, y2;
		
		x1 = FEN.getColumnFromString(a);
		y1 = FEN.getRowFromString(a);
		x2 = FEN.getColumnFromString(b);
		y2 = FEN.getRowFromString(b);
		
		if(x1==-1||y1==-1||x2==-1||y2==-1)
			return null;
		
		return new Move(x1, y1, x2, y2);
	}
}
