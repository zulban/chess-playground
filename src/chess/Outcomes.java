package chess;

public enum Outcomes {
	WHITE_WIN,BLACK_WIN,IN_PROGRESS,STALEMATE,WHITE_RESIGN,BLACK_RESIGN, MAX_TURNS,REPETITION;
}
