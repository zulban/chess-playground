package chess;

public class Piece {

	private static final String fenChars = "-KPQRBNkpqrbn";
	public static final int NO_PIECE = 0,KING_WHITE = 1, PAWN_WHITE = 2, QUEEN_WHITE = 3, ROOK_WHITE = 4, BISHOP_WHITE = 5,
			KNIGHT_WHITE = 6, KING_BLACK = 7, PAWN_BLACK = 8, QUEEN_BLACK = 9, ROOK_BLACK = 10, BISHOP_BLACK = 11,
			KNIGHT_BLACK = 12;

	public static final int getPieceFromFenChar(char c) {
		return fenChars.indexOf(c);
	}
	public static final char getFenCharFromPiece(int piece){
		return fenChars.charAt(piece);
	}
	
	public static int flipPlayerForPiece(int piece){
		//Returns the same piece type but belonging to the other player.
		//A piece like KING_BLACK, returns KING_WHITE
		if(piece==NO_PIECE)
			return NO_PIECE;
		else if(piece>=7)
			return piece-6;
		return piece+6;
	}
	
	public static Player getPlayerFromPiece(int piece){
		if(piece==NO_PIECE)
			return Player.NO_PLAYER;
		else if(piece>=7)
			return Player.BLACK;
		else
			return Player.WHITE;
	}

}
