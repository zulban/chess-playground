package chess;

public enum Player {
WHITE,BLACK,NO_PLAYER;
	
	public static Player flipPlayer(Player player){
		return player==Player.WHITE?Player.BLACK:Player.WHITE;
	}
}
