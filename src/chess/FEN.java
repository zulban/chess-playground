//Significantly adapted and simplified for high school classroom use by Stuart Spence, 2017.

/*
 * Copyright (C) Bernhard Seybold. All rights reserved.
 *
 * This software is published under the terms of the LGPL Software License,
 * a copy of which has been included with this distribution in the LICENSE.txt
 * file.
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 *
 * $Id: FEN.java,v 1.1 2002/12/08 13:27:35 BerniMan Exp $
 */

package chess;

public class FEN {

	public static final String START_POSITION = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1";

	public static int[] getPointFromSquareString(String squareString) {
		return new int[] { getRowFromString(squareString), getColumnFromString(squareString) };
	}
	
	public static char getCharFromX(int x){
		return "abcdefgh".charAt(x);
	}

	public static int getColumnFromString(String squareString) {
		return "abcdefgh".indexOf(squareString.charAt(0));
	}

	public static int getRowFromString(String squareString) {
		return "12345678".indexOf(squareString.charAt(1));
	}
	
	public static Board getBoardWithFen(String fen) throws IllegalArgumentException {
		Board board=new Board();

		int index = 0;
		char ch;

		/* ========== 1st field : pieces ========== */
		int row = 7;
		int col = 0;
		while (index < fen.length() && fen.charAt(index) != ' ') {
			ch = fen.charAt(index);
			if (ch == '/') {
				if (col != 8)
					throw new IllegalArgumentException(
							"Malformatted fen string: unexpected '/' found at index " + index);
				row--;
				col = 0;
			} else if (ch >= '1' && ch <= '8') {
				int num = (int) (ch - '0');
				if (col + num > 8)
					throw new IllegalArgumentException(
							"Malformatted fen string: too many pieces in rank at index " + index + ": " + ch);
				for (int j = 0; j < num; j++) {
					board.setSquare(col, row, Piece.NO_PIECE);
					col++;
				}
			} else {
				int piece = Piece.getPieceFromFenChar(ch);
				if (piece == Piece.NO_PIECE)
					throw new IllegalArgumentException("Malformatted fen string: illegal piece char: " + ch);
				board.setSquare(col, row, piece);
				col++;
			}
			index++;
		}
		if (row != 0 || col != 8)
			throw new IllegalArgumentException("Malformatted fen string: missing pieces at index: " + index);

		/* ========== 2nd field : to play ========== */
		if (index + 1 < fen.length() && fen.charAt(index) == ' ') {
			ch = fen.charAt(index + 1);
			if (ch == 'w')
				board.setPlayerTurn(Player.WHITE);
			else if (ch == 'b')
				board.setPlayerTurn(Player.BLACK);
			else
				throw new IllegalArgumentException(
						"Malformatted fen string: expected 'to play' as second field but found " + ch);
			index += 2;
		}

		/* ========== 3rd field : castles ========== */
		if (index + 1 < fen.length() && fen.charAt(index) == ' ') {
			index++;
			if (fen.charAt(index) == '-') {
				index++;
			} else {
				int last = -1;
				while (index < fen.length() && fen.charAt(index) != ' ') {
					ch = fen.charAt(index);
					if (ch == 'K')
						board.addCastles(Castling.WHITE_KING);
					else if (ch == 'Q')
						board.addCastles(Castling.WHITE_QUEEN);
					else if (ch == 'k')
						board.addCastles(Castling.BLACK_KING);
					else if (ch == 'q')
						board.addCastles(Castling.BLACK_QUEEN);
					else
						throw new IllegalArgumentException(
								"Malformatted fen string: illegal castles identifier or sequence " + ch);
					index++;
				}
			}

		} else {
			//throw new IllegalArgumentException("Malformatted fen string: expected castles at index " + index);
			return board;
		}

		/* ========== 4th field : ep square ========== */
		if (index + 1 < fen.length() && fen.charAt(index) == ' ') {
			index++;
			if (fen.charAt(index) == '-') {
				index++;
			} else {
				if (index + 2 < fen.length()) {
					String squareString = fen.substring(index, index + 2);
					board.setEnPassantSquare(getPointFromSquareString(squareString));
					index += 2;
				}
			}
		} else {
			//throw new IllegalArgumentException("Malformatted fen string: expected ep square at index " + index);
			return board;
		}

		/* ========== 5th field : half move clock ========== */
		if (index + 1 < fen.length() && fen.charAt(index) == ' ') {
			index++;
			int start = index;
			while (index < fen.length() && fen.charAt(index) != ' ')
				index++;
			board.setHalfMoveCounter(Integer.parseInt(fen.substring(start, index)));
		} else {
			//throw new IllegalArgumentException("Malformatted fen string: expected half move clock at index " + index);
			return board;
		}

		/* ========== 6th field : full move number ========== */
		if (index + 1 < fen.length() && fen.charAt(index) == ' ') {
			if (board.getMyPlayer() == Player.WHITE) {
				board.setFullMoveCounter(2 * (Integer.parseInt(fen.substring(index + 1)) - 1));
			} else {
				board.setFullMoveCounter(2 * (Integer.parseInt(fen.substring(index + 1)) - 1) + 1);
			}
		} else {
			//throw new IllegalArgumentException("Malformatted fen string: expected ply number at index " + index);
			return board;
		}
		return board;
	}

	public static String getFEN(Board board) {
		StringBuffer sb = new StringBuffer();

		/* ========== 1st field : pieces ========== */
		int row = 7, col = 0;
		int blanks = 0;
		while (row >= 0) {
			int piece = board.getPiece(col, row);
			if (piece == Piece.NO_PIECE) {
				blanks++;
			} else {
				if (blanks > 0) {
					sb.append(blanks);
					blanks = 0;
				}
				sb.append(Piece.getFenCharFromPiece(piece));
			}
			col++;
			if (col > 7) {
				if (blanks > 0) {
					sb.append(blanks);
					blanks = 0;
				}
				row--;
				col = 0;
				blanks = 0;
				if (row >= 0)
					sb.append('/');
			}
		}

		/* ========== 2nd field : to play ========== */
		sb.append(' ').append(board.getMyPlayer() == Player.WHITE ? 'w' : 'b');

		/* ========== 3rd field : castles ========== */
		sb.append(' ');

		if (board.canCastle(Castling.WHITE_KING))
			sb.append('K');
		if (board.canCastle(Castling.WHITE_QUEEN))
			sb.append('Q');
		if (board.canCastle(Castling.BLACK_KING))
			sb.append('k');
		if (board.canCastle(Castling.BLACK_QUEEN))
			sb.append('q');
		if (!board.canAnyCastle())
			sb.append('-');

		/* ========== 4th field : ep square ========== */
		sb.append(' ');
		if (board.getEnPassantSquare() == null)
			sb.append('-');
		else
			sb.append(board.getEnPassantSquare().toString());

		/* ========== 5th field : half move clock ========== */
		sb.append(' ').append(board.getHalfMoveCounter());

		/* ========== 6th field : full move number ========== */
		sb.append(' ').append(board.getFullMoveCounter() / 2 + 1);

		return sb.toString();
	}
}
