package chess;

import java.util.ArrayList;
import java.util.Arrays;

import ai.Features;
import console.ConsoleColor;
import generic.Utilities;

public class Board {
	private Player playerTurn;
	private int castles, halfMoveCounter, fullMoveCounter;
	private int[][] pieces = new int[8][8];
	private int[] enPassantSquare;

	public static final String startFEN = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1";

	public Board() {
		clear();
	}

	public Board getCopyOfBoard() {
		Board newBoard = new Board();
		newBoard.setPlayerTurn(getMyPlayer());
		newBoard.setCastles(getCastles());
		newBoard.setHalfMoveCounter(getHalfMoveCounter());
		newBoard.setFullMoveCounter(getFullMoveCounter());
		newBoard.setPieces(getCopyOfPieces());
		return newBoard;
	}

	public int[][] getCopyOfPieces() {
		int[][] newPieces = new int[8][8];
		for (int i = 0; i < pieces.length; i++) {
			newPieces[i] = Arrays.copyOf(pieces[i], pieces[i].length);
		}
		return newPieces;
	}

	public void setPieces(int[][] pieces) {
		if (pieces.length != 8 || pieces[0].length != 8)
			throw new IllegalArgumentException("setPieces failed, weird pieces arrays.");
		this.pieces = pieces;
	}

	public Features getFeatures() {
		return new Features(this);
	}
	
	public String toString(boolean useBashColors){
		StringBuffer sb = new StringBuffer();
		String spaces = "      ";
		for (int row = 7; row >= 0; row--) {
			sb.append(spaces);
			sb.append("" + (row + 1) + "   ");
			for (int column = 0; column < 8; column++) {
				int piece=pieces[column][row];
				String c = ""+Piece.getFenCharFromPiece(piece);
				
				if(useBashColors){
					ConsoleColor color=ConsoleColor.GREY;
					if(Piece.getPlayerFromPiece(piece)==Player.WHITE)
						color=ConsoleColor.ORANGE;
					else if(Piece.getPlayerFromPiece(piece)==Player.BLACK)
						color=ConsoleColor.PURPLE;
					sb.append(ConsoleColor.getEscapedColorText(c, color, false)+" ");
				}else{
					sb.append(c+" ");
				}
				
			}
			if (row == 6)
				sb.append(spaces + "turn: " + (playerTurn == Player.WHITE ? "white" : "black"));
			if (row == 5)
				sb.append(spaces + "castles: " + castles);
			if (row == 4 && enPassantSquare != null)
				sb.append(spaces + "en passant: " + enPassantSquare);
			if (row == 3)
				sb.append(spaces + "halfMoveCounter: " + halfMoveCounter);
			if (row == 2)
				sb.append(spaces + "fullMoveCounter: " + fullMoveCounter);

			if (row != 0)
				sb.append('\n');
		}
		sb.append("\n\n" + spaces + "   ");
		for (int i = 0; i < 8; i++)
			sb.append(" " + (char) (i + 97));

		return sb.toString();
	}

	public String toString() {
		return toString(ConsoleColor.canPrintBashColors());
	}

	public void clear() {
		pieces = new int[8][8];
		castles = Castling.NO_CASTLE;
		playerTurn = Player.WHITE;
		enPassantSquare = null;
		fullMoveCounter = 1;
		halfMoveCounter = 0;
	}

	public void move(Move move) {
		move(move.x1, move.y1, move.x2, move.y2);
	}

	public void move(int x1, int y1, int x2, int y2) {
		// Moves the piece at (x1,y1) to (x2,y2).
		// Throws an exception if this is an invalid move: wrong player, no
		// piece, or piece can't move there.

		int piece = pieces[x1][y1];
		if (piece == Piece.NO_PIECE)
			throw new IllegalArgumentException(
					"Cannot move piece '" + piece + "' at (" + x1 + "," + y1 + "), no piece there.");

		Player player = Piece.getPlayerFromPiece(piece);
		if (player != getMyPlayer())
			throw new IllegalArgumentException(
					"Cannot move piece at (" + x1 + "," + y1 + "), not that player's turn. Player = " + player);

		int[][] movesForPiece = getTargetsForPiece(x1, y1);
		if (!Utilities.isPointInPoints(new int[] { x2, y2 }, movesForPiece))
			throw new IllegalArgumentException(
					"Piece at (" + x1 + "," + y1 + ") cannot move to (" + x2 + "," + y2 + ")");

		teleport(x1, y1, x2, y2);

		// Autopromote to a queen.
		if (y2 == 7 && piece == Piece.PAWN_WHITE)
			setSquare(x2, y2, Piece.QUEEN_WHITE);
		if (y2 == 0 && piece == Piece.PAWN_BLACK)
			setSquare(x2, y2, Piece.QUEEN_BLACK);

		setPlayerTurn(Player.flipPlayer(playerTurn));
	}

	public void teleport(int x1, int y1, int x2, int y2) {
		pieces[x2][y2] = pieces[x1][y1];
		pieces[x1][y1] = Piece.NO_PIECE;
	}

	public void setSquare(int x, int y, int piece) {
		pieces[x][y] = piece;
	}

	public void setPlayerTurn(Player player) {
		playerTurn = player;
	}

	public Player getMyPlayer() {
		return playerTurn;
	}

	public Player getOpponentPlayer() {
		return Player.flipPlayer(playerTurn);
	}

	public void addCastles(int castles) {
		this.castles |= castles;
	}

	public boolean canCastle(int castles) {
		return (this.castles & castles) > 0;
	}

	public int getCastles() {
		return castles;
	}

	public void setCastles(int castles) {
		this.castles = castles;
	}

	public boolean canAnyCastle() {
		return castles != Castling.NO_CASTLE;
	}

	public void setEnPassantSquare(int[] square) {
		enPassantSquare = Arrays.copyOf(square, 2);
	}

	public int[] getEnPassantSquare() {
		return enPassantSquare;
	}

	public void setHalfMoveCounter(int counter) {
		halfMoveCounter = counter;
	}

	public void setFullMoveCounter(int plyNumber) {
		fullMoveCounter = plyNumber;
	}

	public int getPiece(int col, int row) {
		return pieces[col][row];
	}

	public int getHalfMoveCounter() {
		return halfMoveCounter;
	}

	public int getFullMoveCounter() {
		return fullMoveCounter;
	}

	public boolean isOccupied(int i, int j) {
		return pieces[i][j] != Piece.NO_PIECE;
	}

	public int[][] getTargetsForPiece(int i, int j) {
		// returns an array of points that a piece of this type and located at
		// (i,j) can move to.
		int piece = pieces[i][j];
		if (piece == Piece.NO_PIECE) {
			return new int[0][2];
		}

		ArrayList<int[]> points = new ArrayList<int[]>();
		Player opponent = Piece.getPlayerFromPiece(Piece.flipPlayerForPiece(piece));

		if (piece == Piece.PAWN_BLACK || piece == Piece.PAWN_WHITE) {
			int direction = piece == Piece.PAWN_BLACK ? -1 : 1;
			int newY = j + direction;
			if (isInsideBoard(i, newY) && pieces[i][newY] == Piece.NO_PIECE)
				points.add(new int[] { i, newY });
			
			//double move for start pawns
			if (piece == Piece.PAWN_BLACK && j == 6 && pieces[i][5]==Piece.NO_PIECE && pieces[i][4]==Piece.NO_PIECE)
				points.add(new int[] { i, j + direction * 2 });
			if (piece == Piece.PAWN_WHITE && j == 1 && pieces[i][2]==Piece.NO_PIECE && pieces[i][3]==Piece.NO_PIECE)
				points.add(new int[] { i, j + direction * 2 });

			for (int xOffset = -1; xOffset < 2; xOffset += 2) {
				int newX = i + xOffset;
				if (isInsideBoard(newX, newY) && Piece.getPlayerFromPiece(pieces[newX][newY]) == opponent)
					points.add(new int[] { newX, newY });
			}
		} else if (piece == Piece.KNIGHT_BLACK || piece == Piece.KNIGHT_WHITE) {
			for (int index = 0; index < Moves.KNIGHT.length; index++) {
				int scanX = i + Moves.KNIGHT[index][0];
				int scanY = j + Moves.KNIGHT[index][1];
				if (isInsideBoard(scanX, scanY)) {
					int target = pieces[scanX][scanY];
					if (target == Piece.NO_PIECE || Piece.getPlayerFromPiece(target) == opponent)
						points.add(new int[] { scanX, scanY });
				}
			}
		} else if (piece == Piece.KING_BLACK || piece == Piece.KING_WHITE) {
			for (int index = 0; index < Moves.KING.length; index++) {
				int scanX = i + Moves.KING[index][0];
				int scanY = j + Moves.KING[index][1];
				if (isInsideBoard(scanX, scanY)) {
					int target = pieces[scanX][scanY];
					if (target == Piece.NO_PIECE || Piece.getPlayerFromPiece(target) == opponent)
						points.add(new int[] { scanX, scanY });
				}
			}
		} else {
			// diagonal and orthogonal moves
			// queen may have both, thus the 'else' above.
			if (piece == Piece.BISHOP_BLACK || piece == Piece.BISHOP_WHITE || piece == Piece.QUEEN_BLACK
					|| piece == Piece.QUEEN_WHITE) {
				for (int direction = 0; direction < Moves.BISHOP.length; direction++) {
					for (int distance = 1; distance < 8; distance++) {
						int scanX = i + Moves.BISHOP[direction][0] * distance;
						int scanY = j + Moves.BISHOP[direction][1] * distance;
						if (isInsideBoard(scanX, scanY)) {
							int target = pieces[scanX][scanY];
							if (target == Piece.NO_PIECE || Piece.getPlayerFromPiece(target) == opponent)
								points.add(new int[] { scanX, scanY });
							if (target != Piece.NO_PIECE)
								break;
						} else {
							break;
						}
					}
				}
			}

			if (piece == Piece.ROOK_BLACK || piece == Piece.ROOK_WHITE || piece == Piece.QUEEN_BLACK
					|| piece == Piece.QUEEN_WHITE) {
				for (int direction = 0; direction < Moves.ROOK.length; direction++) {
					for (int distance = 1; distance < 8; distance++) {
						int scanX = i + Moves.ROOK[direction][0] * distance;
						int scanY = j + Moves.ROOK[direction][1] * distance;
						if (isInsideBoard(scanX, scanY)) {
							int target = pieces[scanX][scanY];
							if (target == Piece.NO_PIECE || Piece.getPlayerFromPiece(target) == opponent)
								points.add(new int[] { scanX, scanY });
							if (target != Piece.NO_PIECE)
								break;
						} else {
							break;
						}
					}
				}
			}
		}

		// filter out points if this move puts their own king in check
		ArrayList<int[]> safePoints = new ArrayList<int[]>();
		for (int index = 0; index < points.size(); index++) {
			int[] point = points.get(index);
			Board testBoard = getCopyOfBoard();
			testBoard.teleport(i, j, point[0], point[1]);
			int testPiece = testBoard.getPiece(point[0], point[1]);
			Player testPlayer = Piece.getPlayerFromPiece(testPiece);
			testBoard.setPlayerTurn(testPlayer);
			if (!testBoard.isCheck())
				safePoints.add(point);
		}

		return safePoints.toArray(new int[safePoints.size()][2]);
	}

	public boolean isInsideBoard(int i, int j) {
		return i >= 0 && i < 8 && j >= 0 && j < 8;
	}

	public int[] findKing(Player player) {
		// returns the position of the king for this player.
		int king = player == Player.WHITE ? Piece.KING_WHITE : Piece.KING_BLACK;
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				if (pieces[i][j] == king)
					return new int[] { i, j };
			}
		}
		throw new IllegalArgumentException("findKing failed, no king!");
	}

	public int[][] findQueens(Player player) {
		ArrayList<int[]> queens = new ArrayList<int[]>();
		int queen = player == Player.WHITE ? Piece.QUEEN_WHITE : Piece.QUEEN_BLACK;
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				if (pieces[i][j] == queen)
					queens.add(new int[] { i, j });
			}
		}
		return queens.toArray(new int[queens.size()][]);
	}

	public boolean isCheck() {
		int[] kingPoint = findKing(getMyPlayer());
		return isPieceBeingAttacked(kingPoint[0], kingPoint[1]);
	}

	public boolean couldSquareBeAttacked(int i, int j, Player attackedPlayer) {
		// if attackedPlayer had a piece at this square, returns true if it
		// could be attacked by the other player.
		// ignores whatever piece is here, as though attackedPlayer had a piece
		// here.
		int oldPiece = pieces[i][j];
		int tempPiece = attackedPlayer == Player.WHITE ? Piece.BISHOP_WHITE : Piece.BISHOP_BLACK;
		pieces[i][j] = tempPiece;
		boolean couldBeAttacked = isPieceBeingAttacked(i, j);
		pieces[i][j] = oldPiece;
		return couldBeAttacked;
	}

	public boolean isPieceBeingAttacked(int i, int j) {
		// returns true if the piece at this coordinate is being attacked by a
		// piece belonging to the other player.
		// kings can be "attacked".
		int flippedPiece = Piece.flipPlayerForPiece(pieces[i][j]);
		Player opponent = Piece.getPlayerFromPiece(flippedPiece);
		int[][] scans = getPiecesThatCanMoveHere(i, j);
		for (int index = 0; index < scans.length; index++) {
			int scanX = scans[index][0];
			int scanY = scans[index][1];
			Player scanPlayer = Piece.getPlayerFromPiece(pieces[scanX][scanY]);
			if (scanPlayer == opponent)
				return true;
		}
		return false;
	}

	public boolean isCheckmate() {
		if (!isCheck())
			return false;

		Player kingPlayer = getMyPlayer();
		int[] kingPoint = findKing(kingPlayer);
		int x = kingPoint[0];
		int y = kingPoint[1];

		int[][] attackers = getAttackers(x, y);
		if (canKingMoveOutOfCheck(x, y) || canKingAttackersBeKilled(x, y, attackers)
				|| canPieceBlockKingAttackers(x, y, attackers))
			return false;

		return true;
	}

	private boolean canPieceBlockKingAttackers(int kingX, int kingY, int[][] attackers) {
		if (attackers.length > 1)
			return false;
		int attackerX = attackers[0][0];
		int attackerY = attackers[0][1];
		int attacker = pieces[attackerX][attackerY];
		Player attackerPlayer = Piece.getPlayerFromPiece(attacker);
		if (attacker != Piece.BISHOP_BLACK && attacker != Piece.BISHOP_WHITE && attacker != Piece.ROOK_BLACK
				&& attacker != Piece.ROOK_WHITE && attacker != Piece.QUEEN_BLACK && attacker != Piece.QUEEN_WHITE)
			return false;

		int[][] points = Utilities.getPointsBetween(kingX, kingY, attackerX, attackerY);
		for (int index = 0; index < points.length; index++) {
			int[][] blockers = getPiecesThatCanMoveHere(points[index][0], points[index][1]);

			// filter out kings and pieces not belonging to this king
			int[] blacklistPieces = new int[] { Piece.KING_BLACK, Piece.KING_WHITE };
			blockers = filterPieces(blockers, blacklistPieces, attackerPlayer);

			if (blockers.length == 0)
				continue;
			for (int blockerIndex = 0; blockerIndex < blockers.length; blockerIndex++) {
				int bx = blockers[blockerIndex][0];
				int by = blockers[blockerIndex][1];
				int blocker = pieces[bx][by];
				if (blocker != Piece.KING_BLACK && blocker != Piece.KING_WHITE && blocker != Piece.NO_PIECE){
					Board blockedBoard = this.getCopyOfBoard();
					blockedBoard.teleport(bx,by,points[index][0],points[index][1]);
					return !blockedBoard.isCheck();
				}
			}
		}
		return false;
	}

	private boolean canKingAttackersBeKilled(int kingX, int kingY, int[][] attackers) {
		if (attackers.length > 1)
			return false;
		int attackerX = attackers[0][0];
		int attackerY = attackers[0][1];
		int[][] defenders = getAttackers(attackerX, attackerY);
		if (defenders.length == 0)
			return false;

		for (int index = 0; index < defenders.length; index++) {
			int defenderX = defenders[index][0];
			int defenderY = defenders[index][1];
			Board future = getCopyOfBoard();
			future.teleport(defenderX, defenderY, attackerX, attackerY);
			if (!future.isCheck())
				return true;
		}
		return false;
	}

	public void print(String text) {
		System.out.println(text);
	}

	private boolean canKingMoveOutOfCheck(int kingX, int kingY) {
		int[][] moves = getTargetsForPiece(kingX, kingY);
		return moves.length > 0;
	}

	public int[][] getAttackers(int i, int j) {
		int[][] allPoints = getPiecesThatCanMoveHere(i, j);

		// filter out results to only include opponents of (i,j)
		Player opponent = Player.flipPlayer(Piece.getPlayerFromPiece(pieces[i][j]));
		ArrayList<int[]> points = new ArrayList<int[]>();
		for (int index = 0; index < allPoints.length; index++) {
			int x = allPoints[index][0];
			int y = allPoints[index][1];
			if (Piece.getPlayerFromPiece(pieces[x][y]) == opponent)
				points.add(new int[] { x, y });
		}
		return points.toArray(new int[points.size()][]);
	}

	public int[][] getPiecesThatCanMoveHere(int i, int j) {
		// Returns the pieces that can move here, if it were that player's turn.
		// A player can "move" to a square occupied by their opponent's king.
		// Will not return pieces if their type is in the filteredPieces array.
		ArrayList<int[]> points = new ArrayList<int[]>();
		int herePiece = pieces[i][j];
		Player herePlayer = Piece.getPlayerFromPiece(herePiece);
		Player opponent = Piece.getPlayerFromPiece(Piece.flipPlayerForPiece(herePiece));

		// knights
		for (int index = 0; index < Moves.KNIGHT.length; index++) {
			int scanX = i + Moves.KNIGHT[index][0];
			int scanY = j + Moves.KNIGHT[index][1];
			if (!isInsideBoard(scanX, scanY))
				continue;
			int scanPiece = pieces[scanX][scanY];
			if (scanPiece == Piece.KNIGHT_BLACK || scanPiece == Piece.KNIGHT_WHITE)
				points.add(new int[] { scanX, scanY });
		}

		// 8 directions outward
		for (int index = 0; index < Moves.KING.length; index++) {
			for (int distance = 1; distance < 8; distance++) {
				int vectorX = Moves.KING[index][0];
				int vectorY = Moves.KING[index][1];
				int scanX = i + vectorX * distance;
				int scanY = j + vectorY * distance;
				if (!isInsideBoard(scanX, scanY))
					break;
				int scanPiece = pieces[scanX][scanY];
				if (scanPiece == Piece.NO_PIECE)
					continue;

				Player scanOpponent = Piece.getPlayerFromPiece(scanPiece);
				if (herePiece != Piece.NO_PIECE && herePlayer == scanOpponent)
					break;
				if ((scanPiece == Piece.KING_BLACK || scanPiece == Piece.KING_WHITE) && distance > 1)
					break;
				boolean isScanDiagonal = vectorX != 0 && vectorY != 0;
				if (isScanDiagonal) {
					boolean isPieceDiagonal = scanPiece == Piece.BISHOP_BLACK || scanPiece == Piece.BISHOP_WHITE
							|| scanPiece == Piece.QUEEN_BLACK || scanPiece == Piece.QUEEN_WHITE
							|| scanPiece == Piece.KING_BLACK || scanPiece == Piece.KING_WHITE;
					if (isPieceDiagonal)
						points.add(new int[] { scanX, scanY });
				} else {
					boolean isPieceOrthogonal = scanPiece == Piece.ROOK_BLACK || scanPiece == Piece.ROOK_WHITE
							|| scanPiece == Piece.QUEEN_BLACK || scanPiece == Piece.QUEEN_WHITE
							|| scanPiece == Piece.KING_BLACK || scanPiece == Piece.KING_WHITE;

					if (isPieceOrthogonal)
						points.add(new int[] { scanX, scanY });
				}

				break;
			}
		}

		// pawn
		for (int yOffset = -1; yOffset < 2; yOffset += 2) {
			for (int xOffset = -1; xOffset < 2; xOffset += 1) {
				int scanX = i + xOffset;
				int scanY = j + yOffset;
				if (!isInsideBoard(scanX, scanY))
					continue;
				int scanPiece = pieces[scanX][scanY];
				if (scanPiece != Piece.PAWN_BLACK && scanPiece != Piece.PAWN_WHITE)
					continue;

				Player correctPawnPlayer = yOffset == 1 ? Player.BLACK : Player.WHITE;
				Player scanPlayer = Piece.getPlayerFromPiece(scanPiece);
				if (scanPlayer == correctPawnPlayer) {
					boolean canPawnWalk = xOffset == 0 && herePiece == Piece.NO_PIECE;
					boolean canPawnCapture = xOffset != 0 && scanPlayer == opponent;
					if (canPawnWalk || canPawnCapture)
						points.add(new int[] { scanX, scanY });
				}
			}
		}

		return points.toArray(new int[points.size()][]);
	}

	public int[][] filterPieces(int[][] points, int[] blacklistPieces) {
		return filterPieces(points, blacklistPieces, Player.NO_PLAYER);
	}

	public int[][] filterPieces(int[][] points, int[] blacklistPieces, Player blacklistPlayer) {
		// Returns a filtered list of points.
		// Removes points that are pieces in blacklistPieces.
		// Removes points that are pieces belonging to blacklistPlayer.
		if (blacklistPieces.length == 0 && blacklistPlayer == Player.NO_PLAYER)
			return points;

		ArrayList<int[]> newPoints = new ArrayList<int[]>();
		for (int index = 0; index < points.length; index++) {
			int x = points[index][0];
			int y = points[index][1];
			int piece = pieces[x][y];
			boolean shouldInclude = Piece.getPlayerFromPiece(piece) != blacklistPlayer;
			for (int filterIndex = 0; filterIndex < blacklistPieces.length; filterIndex++) {
				if (piece == blacklistPieces[filterIndex]) {
					shouldInclude = false;
					break;
				}
			}
			if (shouldInclude)
				newPoints.add(new int[] { x, y });
		}
		return newPoints.toArray(new int[newPoints.size()][]);
	}

	public Move[] getAllMoves() {
		return getAllMoves(playerTurn);
	}

	public Move[] getAllMoves(Player player) {
		ArrayList<Move> moves = new ArrayList<Move>();
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				int piece = pieces[i][j];
				if (piece == Piece.NO_PIECE || Piece.getPlayerFromPiece(piece) != player)
					continue;
				int[][] targets = getTargetsForPiece(i, j);
				for (int index = 0; index < targets.length; index++)
					moves.add(new Move(i, j, targets[index][0], targets[index][1]));
			}
		}
		return moves.toArray(new Move[moves.size()]);
	}

	public boolean isStalemate() {
		if (isCheck())
			return false;
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				int piece = pieces[i][j];
				if (piece == Piece.NO_PIECE || Piece.getPlayerFromPiece(piece) != playerTurn)
					continue;
				if (getTargetsForPiece(i, j).length > 0)
					return false;
			}
		}
		return true;
	}

	public boolean isValidMove(Move move) {
		// Returns true if this is a valid move and the current player owns this
		// piece.
		int piece = pieces[move.x1][move.y1];
		if (Piece.getPlayerFromPiece(piece) != playerTurn || piece == Piece.NO_PIECE)
			return false;

		int[][] targets = getTargetsForPiece(move.x1, move.y1);
		for (int i = 0; i < targets.length; i++) {
			if (targets[i][0] == move.x2 && targets[i][1] == move.y2)
				return true;
		}
		return false;
	}
}
