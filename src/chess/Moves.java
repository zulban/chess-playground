package chess;

public class Moves {
public static final int[][] KNIGHT = {{-2,1},{-1,2},{1,2},{2,1},{2,-1},{1,-2},{-1,-2},{-2,-1}};
public static final int[][] BISHOP = {{1,1},{-1,1},{-1,-1},{1,-1}};
public static final int[][] ROOK = {{-1,0},{0,1},{1,0},{0,-1}};
public static final int[][] KING = {{-1,0},{0,1},{1,0},{0,-1},{1,1},{-1,1},{-1,-1},{1,-1}};
}
