package tests;

import java.util.Arrays;

import generic.Utilities;

public class TestUtilities extends ZUnit  {
	 public void testPointsInBetween(){
		 int[][] points;
		 
		 points=Utilities.getPointsBetween(1, 1, 4, 4);
		 assertTrue(Utilities.isPointInPoints(new int[]{2, 2},points));
		 assertTrue(Utilities.isPointInPoints(new int[]{3, 3},points));
		 assertEqual(points.length,2);
		 
		 points=Utilities.getPointsBetween(1, 1, 1, 4);
		 assertTrue(Utilities.isPointInPoints(new int[]{1, 2},points));
		 assertTrue(Utilities.isPointInPoints(new int[]{1, 3},points));
		 assertEqual(points.length,2,"points = "+Arrays.deepToString(points));
		 
		 points=Utilities.getPointsBetween(6, 4, 3, 1);
		 assertTrue(Utilities.isPointInPoints(new int[]{5, 3},points));
		 assertTrue(Utilities.isPointInPoints(new int[]{4, 2},points));
		 assertEqual(points.length,2,"points = "+Arrays.deepToString(points));
	 }
}
