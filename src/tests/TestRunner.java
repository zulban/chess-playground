package tests;

public class TestRunner {
	public static void main(String[] args){		
		(new TestBoardFeatures()).runAllTests();
		(new TestBoardRules()).runAllTests();
		(new TestUtilities()).runAllTests();
		(new TestMinMax()).runAllTests();
		
		ZUnit.showFinalReport();
	}
}
