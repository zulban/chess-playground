package tests;

import java.util.ArrayList;
import java.util.Arrays;

import ai.Features;
import chess.Board;
import chess.FEN;
import chess.Match;
import chess.Move;
import chess.Piece;
import chess.Player;
import default_bots.EvaluationBot;
import default_bots.TemplateBot;
import generic.Utilities;

public class TestBoardRules extends ZUnit {

	// This tests information about the board according to the rules of chess.
	// Where can pieces move, where can they attack, who is attacked, is anyone
	// in check or checkmate.

	public void testBasicFunctions() {
		Board board = FEN.getBoardWithFen(Board.startFEN);
		assertTrue(board.isOccupied(1, 1));
		assertFalse(board.isOccupied(3, 3));
	}

	public void testSetWithFen() {
		Board board = FEN.getBoardWithFen(Board.startFEN);
		assertEqual(board.getPiece(0, 0), Piece.ROOK_WHITE);
		assertEqual(board.getPiece(7, 7), Piece.ROOK_BLACK);
		assertEqual(board.getPiece(3, 0), Piece.QUEEN_WHITE);
		assertEqual(board.getPiece(4, 0), Piece.KING_WHITE);
	}

	public void testRepetitionDraw() {
		EvaluationBot bot1 = new TemplateBot();
		EvaluationBot bot2 = new TemplateBot();
		Match match = new Match(bot1, bot2, 1, false, false);
		match.setBoard("3K4/2Q5/8/8/6p1/4ppn1/5k2/8 w");
		
		int qx = 2;
		int qy = 6;
		int kx = 5;
		int ky = 1;

		for (int i = 0; i < 2; i++) {
			assertFalse(match.isRepetitionDraw(),"i = "+i);
			
			match.playNextTurn(new Move(qx, qy, qx, qy - 1));
			match.playNextTurn(new Move(kx, ky, kx, ky - 1));
			match.playNextTurn(new Move(qx, qy - 1, qx, qy));
			match.playNextTurn(new Move(kx, ky - 1, kx, ky));
		}
		assertTrue(match.isRepetitionDraw());
	}

	public void testIsValidMove() {
		Board board = FEN.getBoardWithFen(Board.startFEN);
		Move move;

		// pawn up one
		move = new Move(4, 1, 4, 2);
		assertTrue(board.isValidMove(move));

		// knight
		move = new Move(1, 0, 2, 2);
		assertTrue(board.isValidMove(move));

		// move piece that isn't mine
		move = new Move(3, 6, 3, 5);
		assertFalse(board.isValidMove(move));

		// move empty square
		move = new Move(3, 3, 3, 4);
		assertFalse(board.isValidMove(move));
	}

	public void testQueenAutopromote() {
		Board board = FEN.getBoardWithFen("8/2P5/6k1/6pp/8/2N5/1K6/8 w");
		board.move(2, 6, 2, 7);
		assertEqual(board.getPiece(2, 7), Piece.QUEEN_WHITE);
	}

	public void testPawnMovesStart() {
		Board board = FEN.getBoardWithFen(Board.startFEN);
		int[][] points = board.getTargetsForPiece(1, 1);
		assertTrue(Utilities.isPointInPoints(new int[] { 1, 2 }, points));
		assertTrue(Utilities.isPointInPoints(new int[] { 1, 3 }, points), "points = " + Arrays.deepToString(points));
		assertEqual(points.length, 2);

		points = board.getTargetsForPiece(6, 6);
		assertTrue(Utilities.isPointInPoints(new int[] { 6, 5 }, points));
		assertTrue(Utilities.isPointInPoints(new int[] { 6, 4 }, points));
		assertEqual(points.length, 2);
	}

	public void testPawnMovesCaptureNoAdvance() {
		// the pawn can capture but cannot advance
		String fen = "4k3/8/3Rr3/3P4/8/8/K7/8 w";
		Board board = FEN.getBoardWithFen(fen);

		int[][] points = board.getTargetsForPiece(3, 4);
		assertTrue(Utilities.isPointInPoints(new int[] { 4, 5 }, points));
		assertEqual(points.length, 1);

	}

	public void testKnightMovesStart() {
		Board board = FEN.getBoardWithFen(Board.startFEN);
		int[][] points = board.getTargetsForPiece(1, 0);
		assertTrue(Utilities.isPointInPoints(new int[] { 0, 2 }, points));
		assertTrue(Utilities.isPointInPoints(new int[] { 2, 2 }, points));
		assertEqual(points.length, 2);

	}

	public void testKnightMoves2() {
		// Knight can capture, edge of board, can't capture self, empty spaces
		String fen = "4k3/8/B1r5/8/1N6/8/8/3K4 w";
		Board board = FEN.getBoardWithFen(fen);

		int[][] points = board.getTargetsForPiece(1, 3);
		assertTrue(Utilities.isPointInPoints(new int[] { 0, 1 }, points));
		assertTrue(Utilities.isPointInPoints(new int[] { 2, 1 }, points));
		assertTrue(Utilities.isPointInPoints(new int[] { 3, 2 }, points));
		assertTrue(Utilities.isPointInPoints(new int[] { 3, 4 }, points));
		assertTrue(Utilities.isPointInPoints(new int[] { 2, 5 }, points));
		assertEqual(points.length, 5);

	}

	public void testBishopMoves() {
		String fen = "2P1k3/8/4n3/7r/4P1B1/8/4P3/2K5 w";
		Board board = FEN.getBoardWithFen(fen);

		int[][] points = board.getTargetsForPiece(6, 3);
		assertTrue(Utilities.isPointInPoints(new int[] { 5, 4 }, points));
		assertTrue(Utilities.isPointInPoints(new int[] { 4, 5 }, points));
		assertTrue(Utilities.isPointInPoints(new int[] { 5, 2 }, points));
		assertTrue(Utilities.isPointInPoints(new int[] { 7, 2 }, points));
		assertTrue(Utilities.isPointInPoints(new int[] { 7, 4 }, points));
		assertEqual(points.length, 5);

	}

	public void testRookMoves() {
		String fen = "5k2/3p4/5p2/1B1r2p1/3Q4/8/8/2K5 b";
		Board board = FEN.getBoardWithFen(fen);

		int[][] points = board.getTargetsForPiece(3, 4);
		assertTrue(Utilities.isPointInPoints(new int[] { 2, 4 }, points));
		assertTrue(Utilities.isPointInPoints(new int[] { 1, 4 }, points));
		assertTrue(Utilities.isPointInPoints(new int[] { 4, 4 }, points));
		assertTrue(Utilities.isPointInPoints(new int[] { 5, 4 }, points));
		assertTrue(Utilities.isPointInPoints(new int[] { 3, 5 }, points));
		assertTrue(Utilities.isPointInPoints(new int[] { 3, 3 }, points));
		assertEqual(points.length, 6);

	}

	public void testQueenMoves() {
		String fen = "4k3/8/8/3BpP2/3NQP2/3P4/K3n3/8 w";
		Board board = FEN.getBoardWithFen(fen);

		int[][] points = board.getTargetsForPiece(4, 3);
		assertTrue(Utilities.isPointInPoints(new int[] { 4, 4 }, points), "points = " + Arrays.deepToString(points));
		assertTrue(Utilities.isPointInPoints(new int[] { 4, 2 }, points));
		assertTrue(Utilities.isPointInPoints(new int[] { 4, 1 }, points));
		assertTrue(Utilities.isPointInPoints(new int[] { 5, 2 }, points));
		assertTrue(Utilities.isPointInPoints(new int[] { 6, 1 }, points));
		assertTrue(Utilities.isPointInPoints(new int[] { 7, 0 }, points));
		assertEqual(points.length, 6);

	}

	public void testKingMoves() {
		String fen = "4k3/8/8/8/8/Pn6/Kp6/1N6 w";
		Board board = FEN.getBoardWithFen(fen);

		int[][] points = board.getTargetsForPiece(0, 1);
		assertTrue(Utilities.isPointInPoints(new int[] { 1, 1 }, points));
		assertTrue(Utilities.isPointInPoints(new int[] { 1, 2 }, points));
		assertEqual(points.length, 2);

	}

	public void testPiecesThatCanMoveHere() {
		String fen = "4R3/6N1/2Q5/3KP3/8/7B/8/8 w";
		Board board = FEN.getBoardWithFen(fen);

		int[][] points = board.getPiecesThatCanMoveHere(4, 5);
		assertTrue(Utilities.isPointInPoints(new int[] { 2, 5 }, points));
		assertTrue(Utilities.isPointInPoints(new int[] { 3, 4 }, points));
		assertTrue(Utilities.isPointInPoints(new int[] { 4, 4 }, points));
		assertTrue(Utilities.isPointInPoints(new int[] { 4, 7 }, points)); // rook
		assertTrue(Utilities.isPointInPoints(new int[] { 6, 6 }, points));
		assertTrue(Utilities.isPointInPoints(new int[] { 7, 2 }, points)); // bishop
		assertEqual(points.length, 6);
	}

	public void testCheck() {
		String fen = "8/3pk3/8/3P4/4R3/8/K7/8 b";
		Board board = FEN.getBoardWithFen(fen);
		assertTrue(board.isCheck());

		board = FEN.getBoardWithFen(Board.startFEN);
		assertFalse(board.isCheck());
	}

	public void testStalemate() {
		String fen = "8/8/3K1n2/8/8/2r3k1/4r3/8 w";
		Board board = FEN.getBoardWithFen(fen);
		assertTrue(board.isStalemate());

		board = FEN.getBoardWithFen(Board.startFEN);
		assertFalse(board.isStalemate());
	}

	public void testGetAttackers() {
		String fen = "3r4/3q4/1Bn2r2/4p3/R2Q3r/4k3/1b6/2K3R1 w";
		Board board = FEN.getBoardWithFen(fen);

		int[][] points = board.getAttackers(3, 3);
		assertTrue(Utilities.isPointInPoints(new int[] { 1, 1 }, points));
		assertTrue(Utilities.isPointInPoints(new int[] { 2, 5 }, points));
		assertTrue(Utilities.isPointInPoints(new int[] { 3, 6 }, points));
		assertTrue(Utilities.isPointInPoints(new int[] { 4, 2 }, points));
		assertTrue(Utilities.isPointInPoints(new int[] { 4, 4 }, points));
		assertTrue(Utilities.isPointInPoints(new int[] { 7, 3 }, points));
		assertEqual(points.length, 6, "points = " + Arrays.deepToString(points));

	}

	public void testGetAttackers2() {
		String fen = "8/5k2/8/8/3bb3/6P1/7P/3rR2K w";
		Board board = FEN.getBoardWithFen(fen);

		int[][] points = board.getAttackers(4, 3);
		assertTrue(Utilities.isPointInPoints(new int[] { 4, 0 }, points));
		assertEqual(points.length, 1);
	}

	public void testFindMoveCandidates() {
		String fen = "3r4/1bn2B2/2k5/QQ4R1/3PK3/1QN5/8/8";
		Board board = FEN.getBoardWithFen(fen);

		int[][] points = board.getPiecesThatCanMoveHere(3, 4);
		assertTrue(Utilities.isPointInPoints(new int[] { 6, 4 }, points));
		assertTrue(Utilities.isPointInPoints(new int[] { 5, 6 }, points));
		assertTrue(Utilities.isPointInPoints(new int[] { 3, 7 }, points));
		assertTrue(Utilities.isPointInPoints(new int[] { 2, 6 }, points));
		assertTrue(Utilities.isPointInPoints(new int[] { 2, 5 }, points));
		assertTrue(Utilities.isPointInPoints(new int[] { 1, 4 }, points));
		assertTrue(Utilities.isPointInPoints(new int[] { 1, 2 }, points));
		assertTrue(Utilities.isPointInPoints(new int[] { 2, 2 }, points));
		assertTrue(Utilities.isPointInPoints(new int[] { 3, 3 }, points));
		assertTrue(Utilities.isPointInPoints(new int[] { 4, 3 }, points));
		assertEqual(points.length, 10);

	}

	public void testFindMoveCandidatesWithFilter() {
		String fen = "1R2k3/PR6/8/5b2/8/4K3/8/6r1 b";
		Board board = FEN.getBoardWithFen(fen);
		int[] blacklistPieces = new int[] { Piece.KING_BLACK, Piece.KING_WHITE };
		int[][] points = board.getPiecesThatCanMoveHere(2, 7);
		points = board.filterPieces(points, blacklistPieces);
		assertTrue(Utilities.isPointInPoints(new int[] { 5, 4 }, points));
		assertTrue(Utilities.isPointInPoints(new int[] { 1, 7 }, points));
		assertEqual(points.length, 2);
	}

	public void testFilterPiecesFromArray() {
		String fen = "1R2k3/PR6/8/5b2/8/4K3/8/6r1 b";
		Board board = FEN.getBoardWithFen(fen);
		int[] filteredPieces = new int[] { Piece.ROOK_WHITE, Piece.ROOK_BLACK, Piece.KING_BLACK, Piece.KING_WHITE };
		int[][] points = new int[][] { { 0, 6 }, { 1, 6 }, { 1, 7 }, { 4, 2 }, { 4, 7 }, { 5, 4 }, { 6, 0 } };
		int[][] filteredPoints = board.filterPieces(points, filteredPieces);
		assertTrue(Utilities.isPointInPoints(new int[] { 0, 6 }, filteredPoints));
		assertTrue(Utilities.isPointInPoints(new int[] { 5, 4 }, filteredPoints));
		assertEqual(filteredPoints.length, 2, Arrays.deepToString(filteredPoints));
	}

	public void testKingCannotMoveIntoCheck() {
		String fen = "7n/7k/7p/5K2/8/8/8/6R1 b";
		Board board = FEN.getBoardWithFen(fen);
		int[][] moves = board.getTargetsForPiece(6, 7);
		assertEqual(moves.length, 0, Arrays.deepToString(moves));
	}
	
	public void testCannotBlockWithPinnedPieceCheckmate(){
		String fen = "Rn2k3/2Q1b3/5Q2/1B6/8/8/8/4K3 b";
		Board board = FEN.getBoardWithFen(fen);
		assertTrue(board.isCheckmate());
	}

	public void testSmotheredMate() {
		// smothered mate
		String fen = "6rk/6pn/6N1/8/3K4/8/8/8 b";
		Board board = FEN.getBoardWithFen(fen);

		assertTrue(board.isCheckmate());
	}

	public void test2RooksMate() {
		String fen = "2R1k3/2R5/8/8/8/4K1n1/7r/8 b";
		Board board = FEN.getBoardWithFen(fen);

		assertTrue(board.isCheckmate());
	}

	public void test2RooksAlmostMate() {
		// the bishop can take the rook
		String fen = "1R2k3/1R6/8/4b3/8/4K3/8/7r b";
		Board board = FEN.getBoardWithFen(fen);

		assertFalse(board.isCheckmate());
	}

	public void test2RooksAlmostMate2() {
		// the bishop can block the rook
		String fen = "1R2k3/PR6/8/5b2/8/4K3/8/6r1 b";
		Board board = FEN.getBoardWithFen(fen);

		assertFalse(board.isCheckmate());
	}

	public void testPinnedMate() {
		// the rook can capture the checking bishop, but moving it puts you in
		// check
		String fen = "8/5k2/8/8/3bb3/6P1/7P/3rR2K w";
		Board board = FEN.getBoardWithFen(fen);

		assertTrue(board.isCheckmate());
	}

	public void testMoveKingNotMate() {
		// the king just has to move
		String fen = "4R2k/5K2/1p6/8/8/8/8/8 b";
		Board board = FEN.getBoardWithFen(fen);

		assertFalse(board.isCheckmate());
	}

}