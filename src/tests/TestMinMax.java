package tests;

import java.util.Arrays;

import ai.Features;
import chess.Board;
import chess.FEN;
import chess.Match;
import chess.Move;
import default_bots.EvaluationBot;
import default_bots.TemplateBot;

public class TestMinMax extends ZUnit {
	
	public void testObviousCheckMate(){
		//Any bot with a simple isCheckmate()?-10000:10000    should do this one move check mate.
		EvaluationBot bot1=new TemplateBot();
		EvaluationBot bot2=new TemplateBot();
		
		Match match = new Match(bot1,bot2,1,false,false);
		match.setBoard("5K2/2r5/8/p5N1/3PPP1N/1rppp2p/3k4/4b2Q b");
		assertFalse(match.isDraw());
		assertFalse(match.didThisBotWin(bot1));
		assertFalse(match.didThisBotWin(bot2));
		
		match.playNextTurn(new Move(1,2,1,7));
		assertFalse(match.isDraw());
		assertFalse(match.didThisBotWin(bot1));
		assertTrue(match.didThisBotWin(bot2),"\n\n"+match.getBoardString());
	}
	
	public void testObviousCheckMate2(){
		//Any bot with a simple isCheckmate()?-10000:10000    should do this one move check mate.
		EvaluationBot bot1=new TemplateBot();
		EvaluationBot bot2=new TemplateBot();
		
		Match match = new Match(bot1,bot2,5,false,false);
		match.setBoard("7k/3pRRnp/4ppp1/1P5p/5ppK/8/8/8 w");
		assertFalse(match.isDraw());
		assertFalse(match.didThisBotWin(bot1));
		assertFalse(match.didThisBotWin(bot2));
		
		match.playNextTurn();
		
		assertFalse(match.isDraw());
		assertFalse(match.didThisBotWin(bot2));
		assertTrue(match.didThisBotWin(bot1),"\n\n"+match.getBoardString());
	}
}





