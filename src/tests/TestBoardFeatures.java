package tests;

import java.util.Arrays;

import ai.Features;
import chess.Board;
import chess.FEN;

public class TestBoardFeatures extends ZUnit {
	
	public void testPieceCount(){
		Features features = new Features(Board.startFEN);
		
		assertEqual(features.countWhitePawns(),8);
		assertEqual(features.countWhiteKnights(),2);
		assertEqual(features.countWhiteBishops(),2);
		assertEqual(features.countWhiteRooks(),2);
		assertEqual(features.countWhiteQueens(),1);
		
		assertEqual(features.countBlackPawns(),8);
		assertEqual(features.countBlackKnights(),2);
		assertEqual(features.countBlackBishops(),2);
		assertEqual(features.countBlackRooks(),2);
		assertEqual(features.countBlackQueens(),1);
	}
	
	public void testStartupCheckCheckMateSlatemate(){
		Features features = new Features(Board.startFEN);
		
		assertFalse(features.isCheck());
		assertFalse(features.isCheckmate());
		assertFalse(features.isStalemate());
	}
	
	public void testBeelinePawns(){
		String fen="6k1/1n6/5pp1/2P5/1P2P3/8/3K2R1/8 w";
		Features features = new Features(fen);
		
		assertEqual(features.countWhiteBeelinePawns(),2);
		assertEqual(features.countBlackBeelinePawns(),1);
	}
	
	public void testPawnAdvanceTotal(){
		String fen="3bkn2/4pp2/3p4/7P/1P4P1/3P1P2/P1P1P3/3K4 w";
		Features features = new Features(fen);
		
		assertEqual(features.countWhitePawnAdvanceTotal(),9);
		assertEqual(features.countBlackPawnAdvanceTotal(),1);
	}
	
	public void testBeelineDistancePawns(){
		String fen="6k1/1n6/5pp1/2P5/1P2P3/8/3K2R1/8 w";
		Features features = new Features(fen);
		
		assertEqual(features.countWhiteBeelineDistance(),3);
		assertEqual(features.countBlackBeelinePawnDistance(),5);
	}
	
	public void testKingSurrounded(){
		String fen="4B3/4B3/1p3k2/1p6/2K1ppq1/1R6/8/8 w";
		Features features = new Features(fen);
		
		assertEqual(features.countWhiteKingAttackers(1),1);
		assertEqual(features.countWhiteKingAttackers(2),3);
		assertEqual(features.countWhiteKingDefenders(1),1);
		assertEqual(features.countWhiteKingDefenders(2),1);
		
		assertEqual(features.countBlackKingAttackers(1),1);
		assertEqual(features.countBlackKingAttackers(2),2);
		assertEqual(features.countBlackKingDefenders(1),0);
		assertEqual(features.countBlackKingDefenders(2),3);
	}
	
	public void testPawnsNearKing(){
		String fen="8/8/1P2k3/3ppp2/1PPPP3/2K5/6p1/8 w";
		Features features = new Features(fen);
		
		assertEqual(features.countWhitePawnsNearMyKing(1),3);
		assertEqual(features.countWhitePawnsNearMyKing(2),4);
		assertEqual(features.countWhitePawnsNearMyKing(3),5);
		
		assertEqual(features.countBlackPawnsNearOpponentKing(1),3);
		assertEqual(features.countBlackPawnsNearOpponentKing(2),3);
		assertEqual(features.countBlackPawnsNearOpponentKing(3),3);
		assertEqual(features.countBlackPawnsNearOpponentKing(4),4);
	}
	
	public void testPieceAttackedBy(){
		String fen="8/4b3/4b3/p7/PPP5/3Bq3/1KNR4/8 w";
		Features features = new Features(fen);
		
		assertEqual(features.countPiecesAttackedByMe(),2);
		assertEqual(features.countPiecesAttackedByOpponent(),4);
	}
	
	public void testCountMoveableSquares(){
		String fen="k7/4b3/4b3/p7/PPP5/3Bq3/1KNR4/8 w";
		Features features = new Features(fen);
		
		assertEqual(features.countWhiteMoveableSquares(),26);
		assertEqual(features.countBlackMoveableSquares(),39);
	}
	
	public void testKingQueenDistance(){
		String fen="8/4b3/4b3/p6Q/PPP5/3Bq3/1KNR4/8 w";
		Features features = new Features(fen);
		
		assertEqual(features.countDistanceBetweenWhiteQueenAndKing(),9);
	}
	
	public void testMyQueenAttacked(){
		String fen="6k1/6b1/8/8/8/1KQ5/8/8 w";
		Features features = new Features(fen);
		assertTrue(features.isWhiteQueenAttacked());
		
		features = new Features(Board.startFEN);
		assertFalse(features.isWhiteQueenAttacked());
	}
	
	public void testConnectedPawns(){
		String fen="4k3/3npp2/5P2/6q1/P3P2p/1P1P3P/8/2K5 w";
		Features features = new Features(fen);
		assertEqual(features.countWhiteConnectedPawns(),5);
	}
	
	public void testCountCaptures(){
		String fen="4k3/3npp2/5P2/6q1/P3P2p/1P1P3P/8/2K5 w";
		Features features = new Features(fen);
		assertEqual(features.countBlackCaptures(),10);
		assertEqual(features.countWhiteCaptures(),9);
		
		features = new Features(Board.startFEN);
		assertEqual(features.countWhiteCaptures(),0);
		assertEqual(features.countBlackCaptures(),0);
	}
}





