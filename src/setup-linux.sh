#This is a quick setup script for Linux/Debian
#To use it, open up a terminal/console. Next, type this and press enter:
#             bash setup-chess-playground.sh
#

sudo apt-get update -y
sudo apt-get install git eclipse eclipse-jdt openjdk-6-jdk openjdk-6-source openjdk-6-demo openjdk-6-doc openjdk-6-jre-headless openjdk-6-jre-lib -y

if [[ $PWD == *"/chess-playground/"* ]]; then
    echo
    echo "Skipping chess-playground download because it appears this script is running from inside the project folder."
    echo 
else
    git clone https://github.com/Zulban/chess-playground
fi

bash chess-playground.sh demo

cd ../..