package ai;

public class AISettings {
	private int minMaxDepth=5;
	
	public void setMinMaxDepth(int depth){
		if (depth<1 || depth>10)
			throw new IllegalArgumentException();
		minMaxDepth=depth;
	}
}
