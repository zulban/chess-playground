package ai;

import chess.Board;
import chess.Move;
import console.ConsoleColor;
import default_bots.EvaluationBot;

public class TreeSearch {

	public static boolean hasExpired(long expireTime) {
		return System.currentTimeMillis() > expireTime;
	}

	public static MinMaxResult minMax(Board board, EvaluationBot bot, int depth, boolean isMaxPlayer, long expireTime) {
		// the returned MinMaxResult.move is only null if there are no possible
		// moves. returns null only when computation time has expired.

		if (hasExpired(expireTime))
			return null;

		Features features = new Features(board);

		if (depth < 1 || board.isCheckmate() || board.isStalemate())
			return new MinMaxResult(bot.evaluate(features), null, depth);

		Move[] moves = board.getAllMoves();
		float worstScore = isMaxPlayer ? Float.NEGATIVE_INFINITY : Float.POSITIVE_INFINITY;
		MinMaxResult bestResult = new MinMaxResult(worstScore, null, depth);
		if (moves.length == 0) {
			ConsoleColor.printRed("Error: minMax was asked to evaluate a board with zero possible moves.");
			System.out.println(board);
		}

		for (int index = 0; index < moves.length; index++) {
			Board child = board.getCopyOfBoard();
			child.move(moves[index]);

			MinMaxResult mmr = minMax(child, bot, depth - 1, !isMaxPlayer, expireTime);

			if (mmr == null)
				return null;

			mmr.bestMove = moves[index];

			if ((isMaxPlayer && mmr.score > bestResult.score) || (!isMaxPlayer && mmr.score < bestResult.score)) {
				bestResult = mmr;
			}
			
			if ((isMaxPlayer && mmr.score == Float.POSITIVE_INFINITY) || (!isMaxPlayer && mmr.score==Float.NEGATIVE_INFINITY)) {
				return mmr;
			}

			if (mmr.score == bestResult.score && mmr.depth > bestResult.depth) {
				bestResult = mmr;
			}
		}
		return bestResult;
	}
}
