package ai;

import chess.Move;

public class MinMaxResult {
	public float score;
	public Move bestMove;
	public int depth = 0;

	public MinMaxResult(float score, Move bestMove, int depth) {
		this.score = score;
		this.bestMove = bestMove;
		this.depth = depth;
	}

	public String toString() {
		return "MinMaxResult score = " + score + " depth = " + depth + " bestMove = " + bestMove;
	}
}
