package ai;

import chess.Board;
import chess.FEN;
import chess.Piece;
import chess.Player;

public class Features extends FeatureUtilities {

	public Features(Board board) {
		this.board = board;
	}

	public Features(String fen) {
		board = FEN.getBoardWithFen(fen);
	}

	public Player getCurrentPlayer() {
		return board.getMyPlayer();
	}

	public int countWhitePawnAdvanceTotal() {
		// Counts how many forward moves all my pawns combined have made.
		return countPawnAdvanceTotal(Player.WHITE);
	}

	public int countBlackPawnAdvanceTotal() {
		// Counts how many forward moves all opponent pawns combined have made.
		return countPawnAdvanceTotal(Player.BLACK);
	}

	public int countWhiteBeelinePawns() {
		// A beeline pawn has no pieces directly in front of it.
		return getBeelinePawns(Player.WHITE).length;
	}

	public int countBlackBeelinePawns() {
		// A beeline pawn has no pieces directly in front of it.
		return getBeelinePawns(Player.BLACK).length;
	}

	private int countBeelineDistance(Player player) {
		int[][] pawns = getBeelinePawns(player);
		if (pawns.length == 0)
			return -1;

		int invert = player == Player.WHITE ? 7 : 0;
		int best = 8;
		for (int index = 0; index < pawns.length; index++) {
			int distance = Math.abs(pawns[index][1] - invert);
			best = Math.min(best, distance);
		}
		return best;
	}

	public int countWhiteBeelineDistance() {
		// returns the number of spaces the closest beeline pawn needs to move
		// to be promoted.
		return countBeelineDistance(Player.WHITE);
	}

	public int countBlackBeelinePawnDistance() {
		// returns the number of spaces the closest beeline pawn needs to move
		// to be promoted.
		return countBeelineDistance(Player.BLACK);
	}

	public int countWhiteKingDefenders(int distance) {
		// returns the number of friendly pieces near my king that are
		// 'distance' number of steps away from the king.
		return countPiecesNearKing(distance, Player.WHITE, Player.WHITE);
	}

	public int countWhiteKingAttackers(int distance) {
		// returns the number of opponent pieces near my king that are
		// 'distance' number of steps away from the king.
		return countPiecesNearKing(distance, Player.WHITE, Player.BLACK);
	}

	public int countBlackKingDefenders(int distance) {
		// returns the number of opponent pieces near the opponent's king that
		// are 'distance' number of steps away from the king.
		return countPiecesNearKing(distance, Player.BLACK, Player.BLACK);
	}

	public int countBlackKingAttackers(int distance) {
		// returns the number of friendly pieces near the opponent's king that
		// are 'distance' number of steps away from the king.
		return countPiecesNearKing(distance, Player.BLACK, Player.WHITE);
	}

	public int countWhitePawnsNearMyKing(int distance) {
		// returns the number of friendly pawns within 'distance' number of
		// steps away from the king.
		Player player = Player.WHITE;
		int piece = player == Player.WHITE ? Piece.PAWN_WHITE : Piece.PAWN_BLACK;
		return countPiecesNearKing(distance, player, player, piece);
	}

	public int countBlackPawnsNearOpponentKing(int distance) {
		// returns the number of friendly pawns within 'distance' number of
		// steps away from the king.
		Player player = Player.BLACK;
		int piece = player == Player.WHITE ? Piece.PAWN_WHITE : Piece.PAWN_BLACK;
		return countPiecesNearKing(distance, player, player, piece);
	}

	public int countPiecesAttackedByMe() {
		return countPiecesAttackedBy(Player.WHITE);
	}

	public int countPiecesAttackedByOpponent() {
		return countPiecesAttackedBy(Player.BLACK);
	}

	public int countWhiteMoveableSquares() {
		// adds up the number of squares all your pieces can move to. Note: this
		// value can be higher than 64, since more than one piece could move to
		// a single square.
		return countMoveableSquares(Player.WHITE);
	}

	public int countBlackMoveableSquares() {
		// adds up the number of squares all your pieces can move to. Note: this
		// value can be higher than 64, since more than one piece could move to
		// a single square.
		return countMoveableSquares(Player.BLACK);
	}

	public int countDistanceBetweenWhiteQueenAndKing() {
		// returns the euclidean distance between the king and queen.
		// if multiple queens, returns the distance to the first queen that is
		// found.
		// returns -1 if no queen is found.
		Player player = Player.WHITE;
		int[] kingPosition = board.findKing(player);
		int[][] queens = board.findQueens(player);

		if (queens.length == 0)
			return -1;

		return Math.abs(kingPosition[0] - queens[0][0]) + Math.abs(kingPosition[1] - queens[0][1]);
	}

	public boolean isWhiteQueenAttacked() {
		// returns true if any piece is attacking my queen.
		// if many queens, only checks the first one.
		int[][] queens = board.findQueens(Player.WHITE);
		if (queens.length == 0)
			return false;

		return board.isPieceBeingAttacked(queens[0][0], queens[0][1]);
	}

	public int countWhiteConnectedPawns() {
		// connected pawns are pawns in adjacent columns. Note: they aren't
		// necessarily guarding each other, just in adjacent columns.
		int count = 0;
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				if (Piece.getPlayerFromPiece(board.getPiece(i, j)) == Player.WHITE && isPawnConnected(i, j))
					count++;
			}
		}
		return count;
	}

	public int countWhiteCaptures() {
		// returns the number of chess pieces you have captured.
		return countCaptures(Player.WHITE);
	}

	public int countBlackCaptures() {
		// returns the number of chess pieces your opponent has captured.
		return countCaptures(Player.BLACK);
	}

	public int countWhitePawns() {
		return countPieces(Piece.PAWN_WHITE);
	}

	public int countWhiteBishops() {
		return countPieces(Piece.BISHOP_WHITE);
	}

	public int countWhiteKnights() {
		return countPieces(Piece.KNIGHT_WHITE);
	}

	public int countWhiteRooks() {
		return countPieces(Piece.ROOK_WHITE);
	}

	public int countWhiteQueens() {
		return countPieces(Piece.QUEEN_WHITE);
	}

	public int countBlackPawns() {
		return countPieces(Piece.PAWN_BLACK);
	}

	public int countBlackBishops() {
		return countPieces(Piece.BISHOP_BLACK);
	}

	public int countBlackKnights() {
		return countPieces(Piece.KNIGHT_BLACK);
	}

	public int countBlackRooks() {
		return countPieces(Piece.ROOK_BLACK);
	}

	public int countBlackQueens() {
		return countPieces(Piece.QUEEN_BLACK);
	}

	public boolean isCheckmate() {
		return board.isCheckmate();
	}

	public boolean isCheck() {
		return board.isCheck();
	}

	public boolean isStalemate() {
		return board.isStalemate();
	}
}
