package ai;

import java.util.ArrayList;

import chess.Board;
import chess.Piece;
import chess.Player;

public class FeatureUtilities {
	// This class hides a lot of the internal logic for Features.java, so that
	// students can be directed to instead look at Features.java for a list of
	// all methods they need.
	
	protected Board board;

	public void showBoard() {
		System.out.println("********************************************");
		System.out.println(board.toString());
	}

	protected int countPieces(int piece) {
		int count = 0;
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				if (board.getPiece(i, j) == piece)
					count++;
			}
		}
		return count;
	}

	protected int countPawnAdvanceTotal(Player player) {
		int total = 0;
		int match, offset;
		if (player == Player.WHITE) {
			match = Piece.PAWN_WHITE;
			offset = 1;
		} else {
			match = Piece.PAWN_BLACK;
			offset = 6;
		}
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				if (board.getPiece(i, j) == match)
					total += Math.abs(j - offset);
			}
		}
		return total;
	}

	protected int[][] getBeelinePawns(Player player) {
		// returns all beeline pawns belonging to this player.
		ArrayList<int[]> pawns = new ArrayList<int[]>();
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				if (isBeelinePawn(i, j, player))
					pawns.add(new int[] { i, j });
			}
		}
		return pawns.toArray(new int[pawns.size()][]);
	}

	protected boolean isBeelinePawn(int i, int j, Player player) {
		// returns true if this position has a beeline pawn owned by this
		// player.
		int direction = player == Player.WHITE ? 1 : -1;
		int pawn = player == Player.WHITE ? Piece.PAWN_WHITE : Piece.PAWN_BLACK;
		if (board.getPiece(i, j) != pawn)
			return false;
		for (int scanDistance = 1; scanDistance < 8; scanDistance++) {
			int scanY = j + scanDistance * direction;
			if (!board.isInsideBoard(i, scanY))
				break;
			if (board.getPiece(i, scanY) != Piece.NO_PIECE)
				return false;
		}
		return true;
	}

	protected int countPiecesNearKing(int distance, Player kingPlayer, Player piecePlayer) {
		return countPiecesNearKing(distance, kingPlayer, piecePlayer, Piece.NO_PIECE);
	}

	protected int countPiecesNearKing(int distance, Player kingPlayer, Player piecePlayer, int pieceFilter) {
		// returns the number of pieces owned by piecePlayer near the king of
		// kingPlayer
		// if pieceFilter is set to NO_PIECE, returns all pieces for this
		// player.
		if (distance < 1)
			throw new IllegalArgumentException("countPiecesNearKing failed, bad distance = '" + distance + "'");

		int[] kingPosition = board.findKing(kingPlayer);
		int count = 0;

		for (int i = -distance; i <= distance; i++) {
			for (int j = -distance; j <= distance; j++) {
				if (i == 0 && j == 0)
					continue;
				int scanX = kingPosition[0] + i;
				int scanY = kingPosition[1] + j;
				if (!board.isInsideBoard(scanX, scanY))
					continue;
				int piece = board.getPiece(scanX, scanY);
				if ((pieceFilter == Piece.NO_PIECE || pieceFilter == piece)
						&& (Piece.getPlayerFromPiece(piece) == piecePlayer))
					count++;
			}
		}
		return count;
	}

	protected int countPiecesAttackedBy(Player player) {
		// returns the number of pieces owned by the opponent to 'player' that
		// have at least one attacker.
		int count = 0;
		Player defender = Player.flipPlayer(player);
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				int piece = board.getPiece(i, j);
				if (piece == Piece.NO_PIECE)
					continue;
				if (Piece.getPlayerFromPiece(piece) != defender)
					continue;

				if (board.isPieceBeingAttacked(i, j))
					count++;
			}
		}
		return count;
	}

	protected int countMoveableSquares(Player player) {
		return board.getAllMoves(player).length;
	}

	protected boolean isPawnConnected(int i, int j) {
		// returns true if a pawn is here, and there is a pawn in an adjacent
		// column of the same player.
		int piece = board.getPiece(i, j);
		if (piece != Piece.PAWN_BLACK && piece != Piece.PAWN_WHITE)
			return false;
		for (int xOffset = -1; xOffset < 2; xOffset += 2) {
			for (int scanY = 0; scanY < 8; scanY++) {
				int scanX = i + xOffset;
				if (!board.isInsideBoard(scanX, scanY))
					continue;
				if (board.getPiece(scanX, scanY) == piece)
					return true;
			}
		}
		return false;
	}

	protected int countCaptures(Player capturedPlayer) {
		int enemyPieceCount = 0;
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				if (Piece.getPlayerFromPiece(board.getPiece(i, j)) == capturedPlayer)
					enemyPieceCount++;
			}
		}
		return 16 - enemyPieceCount;
	}
}
