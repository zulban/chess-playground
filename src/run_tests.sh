find . -type f -name '*.class' -delete

echo "Compiling Tests."
javac tests/TestRunner.java

OUT=$?
if [ $OUT -eq 0 ];then
	echo "Running tests."
	java tests.TestRunner $1 $2 $3 $4
else
	echo "Not running tests, failed to compile."
fi
