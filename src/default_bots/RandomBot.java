package default_bots;

import ai.Features;

import java.util.Random;

import ai.AISettings;

public class RandomBot implements EvaluationBot {		
	public String getBotName(){
		return "Randomus Maximus";
	}
	
	public String getAuthor(){
		return "Christiaan Huygens";
	}

	public void setup(AISettings settings) {
	}

	public float evaluate(Features features) {
		throw new IllegalArgumentException("evaluate should never be called for RandomBot");
	}
}
