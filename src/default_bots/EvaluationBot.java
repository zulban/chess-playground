package default_bots;

import ai.Features;
import ai.AISettings;

public interface EvaluationBot {	
	public void setup(AISettings settings);	
	public float evaluate(Features boardFeatures);
	public String getBotName();
	public String getAuthor();
	
}
