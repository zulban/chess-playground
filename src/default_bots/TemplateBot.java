package default_bots;

import ai.Features;
import chess.Player;
import ai.AISettings;

public class TemplateBot implements EvaluationBot {

	public String getBotName() {
		return "Some unnamed bot!";
	}

	public String getAuthor() {
		return "Unknown author!";
	}

	public void setup(AISettings settings) {

	}

	public float evaluate(Features features) {
		if (features.isCheckmate())
			return features.getCurrentPlayer()==Player.WHITE?Float.NEGATIVE_INFINITY:Float.POSITIVE_INFINITY;
		
		float score=0;
		
		score+=features.countWhiteBishops() * 30;
		score+=features.countWhiteKnights() * 30;
		score-=features.countBlackQueens() * 100;
		
		return score;
	}
}
