package default_bots;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;

public class BotLoader {

	public static EvaluationBot getBotFromPath(String path) {
		System.out.println("Getting EvaluationBot from path: '"+path+"'");
		File file = new File(path);

		try {
			java.net.URL url = file.toURI().toURL();
			URL[] urls = new URL[] { url };
			ClassLoader classLoader = new URLClassLoader(urls);

			String classPath = path.replaceAll(".java", "").replace(File.separatorChar, '.');
			Class cls = classLoader.loadClass(classPath);
			Object newObject = cls.newInstance();
			if (newObject instanceof EvaluationBot)
				return (EvaluationBot) newObject;
		} catch (MalformedURLException | ClassNotFoundException | InstantiationException | IllegalAccessException e) {
			System.out.println("Failed to load bot: '" + path + "'");
			e.printStackTrace();
		}
		return null;
	}

	public static ArrayList<EvaluationBot> getBotsFromFolder(String folder) {
		ArrayList<EvaluationBot> bots = new ArrayList<EvaluationBot>();

		File[] files = new File(folder).listFiles();

		for (int i = 0; i < files.length; i++) {
			File file = files[i];
			if (file.isFile()) {
				String path = file.getPath();
				if (path.endsWith(".java")) {
					EvaluationBot bot = getBotFromPath(path);
					if (bot != null)
						bots.add(bot);
				}
			}
		}
		return bots;
	}
}