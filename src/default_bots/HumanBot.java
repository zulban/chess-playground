package default_bots;

import ai.Features;

import java.util.Random;

import ai.AISettings;

public class HumanBot implements EvaluationBot {	
	public String getBotName(){
		return "Human";
	}
	
	public String getAuthor(){
		return "Human";
	}

	public void setup(AISettings settings) {
	}

	public float evaluate(Features features) {
		throw new IllegalArgumentException("evaluate should never be called for HumanBot");
	}
}
