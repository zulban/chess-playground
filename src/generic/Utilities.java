package generic;

import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

import chess.FEN;
import chess.Move;

public class Utilities {

	public static int clamp(int value, int min, int max) {
		if (value < min)
			return min;
		if (value > max)
			return max;
		return value;
	}
	
	public static String getTruncatedBotNameForFilename(String label){
		String replaced=label.replaceAll("[^a-zA-Z0-9]", "");
		return replaced.substring(0, 10);
	}

	public static boolean compareArrays(int[] array1, int[] array2) {
		// Returns true if arrays are value equal.
		if (array1.length != array2.length)
			return false;
		for (int i = 0; i < array1.length; i++) {
			if (array1[i] != array2[i])
				return false;
		}
		return true;
	}

	public static int[][] getPointsBetween(int x1, int y1, int x2, int y2) {
		// given two coordinates, returns a list of (x,y) for all points in
		// between, not including, those points
		// only works if a rook or a bishop can move from (x1,y1) to (x2,y2).
		ArrayList<int[]> points = new ArrayList<int[]>();

		int xDirection = 0;
		if (x1 < x2)
			xDirection = 1;
		else if (x1 > x2)
			xDirection = -1;

		int yDirection = 0;
		if (y1 < y2)
			yDirection = 1;
		else if (y1 > y2)
			yDirection = -1;

		for (int i = 1; i < 8; i++) {
			int scanX = x1 + xDirection * i;
			int scanY = y1 + yDirection * i;
			if (scanX == x2 && scanY == y2)
				break;
			points.add(new int[] { scanX, scanY });
		}
		return points.toArray(new int[points.size()][]);
	}

	public static boolean isPointInPoints(int[] point, int[][] points) {
		// points must be two dimensional
		if (points == null)
			return false;
		for (int i = 0; i < points.length; i++)
			if (point[0] == points[i][0] && point[1] == points[i][1])
				return true;
		return false;
	}

}
