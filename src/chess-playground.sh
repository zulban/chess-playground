find . -type f -name '*.class' -delete

echo "Compiling Chess Playground."
find . -name "*.java" -print | xargs javac

OUT=$?
if [ $OUT -eq 0 ];then
	echo "Running Chess Playground."
	java console.ChessPlayground $1 $2 $3 $4 $5 $6 $7 $8 $9 $10 $11 $12 $13 $14 $15
else
	echo "Not running Chess Playground, failed to compile."
fi
