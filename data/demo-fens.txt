#This file has a bunch of random chess board setups.
#They are written in FEN. FEN is a notation to describe how a chess board is set up.

#This is the standard board starting setup for chess.
rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1

#Three random boards with 16 pieces.
3B4/K3b3/1r3pP1/8/2P1P2P/1kN3Pr/2p4R/1n6 w - - 0 1
1N6/2p2K1P/B2R3P/5k2/QP2p3/1p2Pqr1/2p5/8 w - - 0 1
2Q1b3/1B2P3/2PP3p/8/1k1K2PP/p4R2/1P6/b1q5 w - - 0 1

#Three random board with 6 pieces.
8/6r1/5K2/8/8/2P2b2/1k1p4/8 w - - 0 1
1q6/7P/8/3K4/8/2R5/5P2/6k1 w - - 0 1
4K3/8/8/P4k2/4R3/3rq3/8/8 w - - 0 1